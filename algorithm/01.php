<?php

/**
 * 冒泡排序
 * 
 * 思路分析：法如其名，就是像冒泡一样，每次从数组当中 冒一个最大的数出来。 
 *     比如：2,4,1    // 第一次 冒出的泡是4 
 *          2,1,4   // 第二次 冒出的泡是 2 
 *          1,2,4   // 最后就变成这样 
 */
$array = array(1, 6, 2, 4, 8, 5);

$num = count($array); //6

for ($i = 0; $i < $num; $i++) {
    for ($j = $num - 1; $j > $i; $j--) {
        if ($array[$j] < $array[$j - 1]) {
            $tmp = $array[$j];
            $array[$j] = $array[$j - 1];
            $array[$j - 1] = $tmp;
        }
    }
}

//写法二：
//for ($i = 0; $i < $num; $i++) {
//    for ($j = 0; $j < $num - 1; $j++) {
//        if ($array[$j] > $array[$j + 1]) {
//            $tmp = $array[$j];
//            $array[$j] = $array[$j + 1];
//            $array[$j + 1] = $tmp;
//        }
//    }
//}


print_r($array);
