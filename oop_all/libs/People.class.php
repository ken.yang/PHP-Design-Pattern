<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/3/6 0006
 * Time: 上午 10:31
 */

class People{
	use Helper;

	private $name;
	private $age;

	/**
	 * 设置姓名
	 *
	 * @param $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * 设置年龄
	 *
	 * @param $age
	 */
	public function setAge($age) {
		$this->age = $age;
	}

	/**
	 * 输出人类信息
	 */
	public function showInfo() {
		echo sprintf("My name is %s,my age is %d", $this->name, $this->age);
	}
}