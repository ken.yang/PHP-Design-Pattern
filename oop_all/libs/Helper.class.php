<?php

/**
 * 帮助类.
 * User: Ken
 * Date: 2016/3/6 0006
 * Time: 上午 10:03
 */
trait Helper {

	/**
	 * 显示当前时间
	 */
	public function showTime() {
		echo date("Y-m-d H:i", time());
	}

	/**
	 * 一个简单的断点调试
	 * @param $data	 一个需要打印的数据
	 */
	public function dd($data) {
		print_r($data);
		die();
	}
}