<?php
/**
 * PHP面向对象练习,入口文件.
 * User: Ken
 * Date: 2016/3/6 0006
 * Time: 上午 10:00
 * Link: http://php.net/manual/zh/language.oop5.php
 */

//自动加载代码
require_once dirname(__FILE__) . '/core/autoload.php';

$people = new People();

$people->setName("ken");
$people->setAge(24);
$people->showInfo();

$people->showTime();