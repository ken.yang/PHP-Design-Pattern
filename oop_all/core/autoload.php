<?php
/**
 * 自动加载类.
 *
 * 类的自动加载spl_autoload_register，匿名函数
 *
 * User: Ken
 * Date: 2016/3/6 0006
 * Time: 上午 10:05
 */

spl_autoload_register(function ($class) {
	$basePath = dirname(dirname(__FILE__));
	if (file_exists($basePath . '/libs/' . $class . '.class.php')) {
		include $basePath . '/libs/' . $class . '.class.php';
	}
});