<?php
/**
 * PHP自带的安全过滤，方便简单.
 * User: Ken
 * Date: 2016/2/28 0028
 * Time: 下午 9:22
 */

$name = 'ken';
$age = 29;
$info = array(
	'height' => 130,
	'weight' => 23
);
$is_man = true;

//INT验证
if (filter_var($age, FILTER_VALIDATE_INT)) {
	echo '是int值';
} else {
	echo '不是int值';
}

//String验证
if (filter_var($is_man, FILTER_VALIDATE_BOOLEAN)) {
	echo '是bool值';
} else {
	echo '不是bool值';
}

//数组验证
if (is_array($info)) {
	echo '这是一个数组';
} else {
	echo '这不是一个数组';
}

