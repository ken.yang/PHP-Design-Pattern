<?php
/**
 * PHP自带hash对密码加密.
 * User: Ken
 * Date: 2016/2/28 0028
 * Time: 下午 9:17
 */

$password = '123456';

//加密password_hash
$password_hash = password_hash($password, PASSWORD_DEFAULT);
$password_hash2 = password_hash($password, PASSWORD_DEFAULT);

echo $password_hash . '<br>';
echo $password_hash2 . '<br>';

//验证密码password_verify
if (password_verify($password, $password_hash)) {
	echo 'password is right<br>';
} else {
	echo 'password is wrong<br>';
}

if (password_verify($password, $password_hash2)) {
	echo 'password is right<br>';
} else {
	echo 'password is wrong<br>';
}