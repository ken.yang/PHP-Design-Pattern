<?php

/**
 * 多态.
 *
 * 描述：Java中的多态是相同方法的不同结果，重载实现多态，但是PHP中没有重载，同一个类中不能存在相同的方法，
 * 所以PHP中没有完全的多态性，PHP可以用代码简单实现类似多态的作用。
 *
 * User: Ken
 * Date: 2016/2/27 0027
 * Time: 下午 7:12
 */
class People {
	public function say() {
		echo 'people will say hello';
	}
}

class Student extends People {
	public function showInfo() {
		echo 'I am a student';
	}
}

class Teacher extends People {
	public function showInfo() {
		echo 'I am a Teacher';
	}
}

class Role {
	public function show($role) {
		$role->showInfo();
	}
}

$role = new Role();
$student = new Student();
$teacher = new Teacher();

$role->show($student);
$role->show($teacher);

