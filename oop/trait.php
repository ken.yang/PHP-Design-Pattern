<?php

/**
 * Trait PHP5.4以后新增的一个新特性.
 * User: Ken
 * Date: 2016/3/6 0006
 * Time: 上午 9:29
 * Link: http://php.net/manual/zh/language.oop5.traits.php
 * ToDo: 增加更多实例
 */
trait Helper {
	public function dd($data) {
		print_r($data);
		die();
	}

	public function showTime() {
		echo date("Y-m-d H:i\n", time());
	}
}

class main {
	use Helper;

	public function index() {
		$data = "I am a string data";
		echo $data;
	}
}

$inti = new main();
$inti->index();
$inti->showTime();
