<?php

/*
 * 单列模式
 * 
 * 
 * $_instance必须声明为静态的私有变量
 * 构造函数和析构函数必须声明为私有,防止外部程序new类从而失去单例模式的意义
 * getInstance()方法必须设置为公有的,必须调用此方法以返回实例的一个引用
 * ::操作符只能访问静态变量和静态函数
 * new对象都会消耗内存
 * 
 * 使用场景:最常用的地方是数据库连接。 
 * 使用单例模式生成一个对象后，
 * 该对象可以被其它众多对象所使用。
 */

class Singleton {

    private static $_instance;

    private function __construct() {
        echo '我在构造方法内,无论多少次调用,但是我只会输出一次<br>';
    }

    public function __clone() {
        trigger_error('该类不能被克隆', E_USER_ERROR);
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function sayHello() {
        echo 'hello<br>';
    }

}
