<?php
/*
 * 单列模式
 * 
 * 只需要实例化一次
 */
?>
<h1>单列模式</h1>
<p>以下内容为单列模式执行结果：</p>
<?php
require_once 'Singleton.php';

//进行多次调用
$singleton = Singleton::getInstance();
$singleton->sayHello();

$singleton2 = Singleton::getInstance();
$singleton2->sayHello();

$singleton3 = Singleton::getInstance();
$singleton3->sayHello();

//实例化(错误提示)
//Fatal error: Call to private Singleton::__construct() from invalid context
//$singletonNew = new Singleton();
//$singletonNew->sayHello();

//克隆(错误提示)
//Fatal error: 该类不能被克隆 in
//$singletonClon = clone $singleton;