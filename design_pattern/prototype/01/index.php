<?php
/*
 * 普通原型模式
 * 
 * 普通原型模式,从个体进行克隆复制
 * 每次复制都不会执行__construct()
 */
?>
<h1>原型模式</h1>
<p>以下内容为普通原型模式执行结果：</p>
<?php
require_once 'client.php';

new client();