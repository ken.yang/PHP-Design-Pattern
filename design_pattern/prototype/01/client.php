42<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'IPrototype.php';

class client {

    //用于直接实例化
    private $fly1;
    private $fly2;
    //用于克隆
    private $cFly1;
    private $cFly2;

    public function __construct() {
        //实例化
        $this->fly1 = new MaleProto();
        $this->fly2 = new FemaleProto();

        //克隆
        $this->cFly1 = clone $this->fly1;
        $this->cFly2 = clone $this->fly2;

        $this->updatedCloneFly = clone $this->fly2;

        //更新克隆
        $this->cFly1->mated = 'true';
        $this->cFly2->fecundity = "186";

        $this->updatedCloneFly->fecundity = 'purple';
        $this->updatedCloneFly->eyeColor = '220';
        $this->updatedCloneFly->wingBeat = '750';
        $this->updatedCloneFly->unitEyes = '92';

        $this->showFly($this->cFly1);
        $this->showFly($this->cFly2);
        $this->showFly($this->updatedCloneFly);
    }

    private function showFly(IPrototype $fly) {
        echo "Eye color is:" . $fly->eyeColor . '<br>';
        echo "wingBeat is:" . $fly->wingBeat . '<br>';
        echo "unitEyes is:" . $fly->unitEyes . '<br>';
        echo "gender is:" . $fly::gender . '<br>';
    }

}
