<?php

/*
 * 接口
 */

abstract class IPrototype {

    public $eyeColor;
    public $wingBeat;
    public $unitEyes;

    abstract function __clone();
}

/**
 * 实现
 */
class MaleProto extends IPrototype {

    const gender = "MALE";

    public $mated;

    public function __construct() {
        $this->eyeColor = 'red';
        $this->wingBeat = '220';
        $this->unitEyes = '760';
    }

    function __clone() {
        
    }

}

class FemaleProto extends IPrototype {

    const gender = "FEMALE";

    public $fecundity;

    public function __construct() {
        $this->eyeColor = 'red';
        $this->wingBeat = '220';
        $this->unitEyes = '760';
    }

    function __clone() {
        
    }

}
