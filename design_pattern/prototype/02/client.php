<?php

/*
 * 客户端
 */
require_once 'IAcmePrototype.php';
require_once 'Marketing.php';
require_once 'Management.php';
require_once 'Engineering.php';

class client {

    private $market;
    private $manage;
    private $engineer;

    public function __construct() {
        $this->makeCoonProto();

        $Tess = clone $this->market;
        $this->setEmployee($Tess, "Tess Smith", 101, "ts101-1234", "tess.png");
        $this->showEmployee($Tess);

        $Ken = clone $this->market;
        $this->setEmployee($Ken, "ken", 103, "ts123456789", "ken.png");
        $this->showEmployee($Ken);
    }

    /**
     * 实例化对象
     */
    private function makeCoonProto() {
        $this->market = new Marketing();
        $this->manage = new Management();
        $this->engineer = new Engineering();
    }

    /**
     * 展示员工信息
     * @param IAcmePrototype $employeeNow
     */
    private function showEmployee(IAcmePrototype $employeeNow) {
        $px = $employeeNow->getPic();
        echo "<img src=$px width='150' height='150' /><br/>";
        echo $employeeNow->getName() . "<br>";
        echo $employeeNow->getDept() . ':' . $employeeNow::UNIT . "<br>";
        echo $employeeNow->getID() . "<br>";
    }

    /**
     * 设置员工信息
     * @param IAcmePrototype $employeeNow
     * @param type $nm
     * @param type $dp
     * @param type $id
     * @param type $px
     */
    private function setEmployee(IAcmePrototype $employeeNow, $nm, $dp, $id, $px) {
        $employeeNow->setName($nm);
        $employeeNow->setDept($dp);
        $employeeNow->setID($id);
        $employeeNow->setPic($px);
    }

}
