<?php
/*
 * OOP原型模式
 * 
 * OOP原型模式,一个基础接口类，部门之间成员的克隆
 * 每次复制都不会执行__construct()
 */
?>
<h1>OOP原型模式</h1>
<p>以下内容为OOP原型模式执行结果：</p>
<?php
require_once 'client.php';

new client();
