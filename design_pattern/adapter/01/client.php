<?php

/*
 * 客户端
 */

require_once 'EuroAdapter.php';
require_once 'DollarCalc.php';

class client {

    private $requestNow;
    private $dollarRequest;

    public function __construct() {
        $this->requestNow = new EuroAdapter();
        $this->dollarRequest = new DollarCalc();

        echo "欧元:" . $this->markAdapterRequest($this->requestNow) . "<br>";
        echo "美元:$" . $this->markDollarRequest($this->dollarRequest);
    }

    private function markAdapterRequest(ITarget $req) {
        return $req->requestCalc(40, 50);
    }

    private function markDollarRequest(DollarCalc $req) {
        return $req->requestCalc(40, 50);
    }

}
