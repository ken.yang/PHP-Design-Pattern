<?php

/*
 * 欧元支付
 */

class EuroCalc {

    private $euro;
    private $product;
    private $service;
    protected $rate = 1; //这里为public或者protected，不然继承的时候无法修改

    public function requestCalc($productNow, $serviceNow) {
        $this->product = $productNow;
        $this->service = $serviceNow;
        $this->euro = $this->product + $this->service;
        return $this->requestTotal();
    }

    private function requestTotal() {
        $this->euro *= $this->rate;
        return $this->euro;
    }

}
