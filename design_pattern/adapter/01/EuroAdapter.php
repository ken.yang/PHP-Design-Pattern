<?php

/*
 * 适配器
 */

require_once 'ITarget.php';
require_once 'EuroCalc.php';

class EuroAdapter extends EuroCalc implements ITarget {

    public function __construct() {
        $this->requester();
    }

    function requester() {
        $this->rate = 0.8111;
    }

}
