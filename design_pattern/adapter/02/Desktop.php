<?php

/*
 * PC端
 */

require_once 'IFormat.php';

class Desktop implements IFormat {

    public function formatHeader() {
        echo 'I am desk page header<br>';
    }

    public function formatContent() {
        echo 'I am desk page content<br>';
    }

    public function formatFooter() {
        echo 'I am desk page footer<br>';
    }

}
