<?php

/*
 * 接口
 */

interface IFormat {

    public function formatHeader();

    public function formatContent();

    public function formatFooter();
}
