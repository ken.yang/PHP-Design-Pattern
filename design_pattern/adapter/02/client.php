<?php

/*
 * 客户端
 */

require_once 'Desktop.php';
require_once 'Mobile.php';
require_once 'MobileAdadpter.php';

class client {

    private $desktop;
    private $mobile;
    private $mobileAdadpter;

    public function __construct() {
        //手机端
        $this->mobile = new Mobile();
        $this->mobileAdadpter = new MobileAdadpter($this->mobile);
        $this->mobileAdadpter->formatHeader();
        $this->mobileAdadpter->formatContent();
        $this->mobileAdadpter->formatFooter();

        //pc端
        $this->desktop = new Desktop();
        $this->desktop->formatHeader();
        $this->desktop->formatContent();
        $this->desktop->formatFooter();
    }

}
