<?php

/*
 * 手机端接口
 */

interface IMobileFormat {

    public function formatHeader();

    public function formatContent();

    public function formatSidebar();
}
