<?php

/*
 * 手机端适配器
 */

require_once 'Iformat.php';
require_once 'Mobile.php';

class MobileAdadpter {

    private $mobile;

    public function __construct(IMobileFormat $mobileNow) {
        $this->mobile = $mobileNow;
    }

    public function formatHeader() {
        $this->mobile->formatHeader();
    }

    public function formatContent() {
        $this->mobile->formatContent();
    }

    public function formatFooter() {
        $this->mobile->formatSidebar();
    }

}
