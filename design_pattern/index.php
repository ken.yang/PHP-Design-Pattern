<h2>PHP设计模式学习</h2>

<p>工厂模式：</p>
<a href="factory/01">普通工厂模式</a><br>
<a href="factory/01">新工厂模式</a>

<p>原型模式：</p>
<a href="prototype/01">普通原型模式</a><br>
<a href="prototype/02">OOP原型模式</a>

<p>适配器模式：</p>
<a href="adapter/01">使用继承的适配器模式</a><br>
<a href="adapter/02">使用组合的适配器模式</a>

<p>单列模式：</p>
<a href="singleton/01">单列模式</a><br>