##PHP设计模式

这是我学习PHP设计模式做的一些笔记记录，根据《Learning PHP设计模式》学习而来。


###其他推荐

如果想学习更多更全的设计模式，推荐

刘伟老师的Java设计模式教程,[刘伟老师的CSDN博客](http://blog.csdn.net/lovelion/article/details/17517213)

四人帮Gof前辈的关于设计模式的书籍