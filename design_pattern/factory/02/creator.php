<?php

/**
 * 接口
 */
abstract class Creator {

    protected abstract function factoryMethod(Product $product);

    public function startFactory($productNow) {
        $countryProduct = $productNow;
        $msg = $this->factoryMethod($countryProduct);
        echo $msg;
    }

}

/**
 * 继承并输出
 */
class CountryFactory extends Creator {

    private $country;

    protected function factoryMethod(Product $product) {
        $this->country = $product;
        return $this->country->getProductInfo();
    }

}
