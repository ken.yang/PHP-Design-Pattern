<?php

/**
 * 产品接口
 */
interface Product {

    public function getProductInfo();
}

/**
 * 文字产品类
 */
class TextProduct implements Product {

    public function getProductInfo() {
        return 'this is a text product<br>';
    }

}

/**
 * 视频产品类
 */
class VideoProduct implements Product {

    public function getProductInfo() {
        return 'this is a video product<br>';
    }

}
