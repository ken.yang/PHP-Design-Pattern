<?php
/*
 * 新工厂模式
 * 
 * 新工厂模式比普通工厂模式的不同在于客户端采用传参执行，一个工厂可以执行N个产品
 * 这样的情况下，就只需要关注增加新产品就行了，客户端通过传参数调用新的产品
 */
?>
<h1>新工厂模式</h1>

<label>原理：</label>
<p>新工厂模式比普通工厂模式的不同在于客户端采用传参执行，一个工厂可以执行N个产品
    ,这样的情况下，就只需要关注增加新产品就行了，客户端通过传参数调用新的产品</p>


<p>以下内容为新工厂模式执行结果：</p>
<?php
include_once 'client.php';

new Client();
