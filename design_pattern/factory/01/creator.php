<?php

/*
 * 工厂模式:创建工厂
 */
include_once 'product.php';

/**
 * 接口(抽象类)
 */
abstract class Creator {

    protected abstract function factoryMethod();

    public function startFactory() {
        $msg = $this->factoryMethod();
        echo $msg;
    }

}

/**
 * 文本工厂
 */
class TextFactory extends Creator {

    protected function factoryMethod() {
        $product = new TextProduct();
        return $product->getProductInfo();
    }

}

/**
 * 视频工厂
 */
class VideoFactory extends Creator {

    protected function factoryMethod() {
        $product = new VideoProduct();
        return $product->getProductInfo();
    }

}
