<?php

/*
 * 客户端调用类,入口
 */
include_once 'creator.php';

class Client {

    public function __construct() {
        echo 'We have a new product<br>';

        $TextFactory = new TextFactory();
        echo $TextFactory->startFactory();

        $VideoFactory = new VideoFactory();
        echo $VideoFactory->startFactory();
    }

}