<?php

/*
 * 客户端
 */

function __autoload($className) {
    $path = $className . '.class.php';
    require_once $path;
}

$stack = new Stack();

$stack->push('hello');
$stack->push('good');
$stack->push('nice');
print_r($stack->getData());

$stack->pop();

print_r($stack->getData());