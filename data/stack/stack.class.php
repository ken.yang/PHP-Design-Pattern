<?php

/**
 * php数据结构练习：栈
 *
 * @author ken
 * @time 2016-02-20<695093513@qq.com>
 * 
 */
class Stack {

    //定义栈和栈的长度
    private $_data = array();
    private $_end = null;

    /**
     * push进栈操作
     * @param string $data
     */
    public function push($data) {
        if ($this->_end === null) {
            $this->_end = 0;
        } else {
            $this->_end++;
        }
        $this->_data[$this->_end] = $data;
    }

    /**
     * pop出栈操作
     * @return boolean
     */
    public function pop() {
        if (empty($this->_data)) {
            return false;
        }

        $result = $this->_data[$this->_end];

        //末端的数据已近删除了，所以长度和数据都应该减一
        array_splice($this->_data, $this->_end);
        $this->_end--;

        return $result;
    }

    public function getData() {
        return $this->_data;
    }

}
