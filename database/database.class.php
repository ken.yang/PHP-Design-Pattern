<?php

/**
 * Mysql链接类
 *
 * @author ken
 * @time 2016-02-21<695093513@qq.com>
 * @todo 增加读写分离功能
 */
class mysql {

    private static $_instance;
    private static $read_and_write = 1;

    public function __construct() {
        echo 'mysql单列模式<br>';
    }

    public function __clone() {
        die('该类不允许克隆');
    }

    /**
     * 数据库连接和查询操作
     * @param string $sql
     * @param string $type
     */
    public function connect($sql, $type = 'default') {
        switch ($type) {
            case 'write':
                echo '<br>这是读写分离的写</br>';
                break;
            case 'read':
                echo '<br>这是读写分离的读</br>';
                break;
            default:
                echo '<br>没有使用读写分离</br>';
                break;
        }
    }

    /**
     * 数据库查询操作
     * @param string $sql
     * @param string $type
     */
    public function query($sql, $type) {
        $config = require_once 'database.config.php';

        if (isset($config['default'])) {
            self::$read_and_write = 0;
        } else {
            self::$read_and_write = 1;
        }

        if (self::$read_and_write == 1) {
            $this->connect($sql, $type);
        } else {
            $this->connect($sql);
        }
    }

    /**
     * 单列构造
     * @return obj
     */
    public static function instance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * 查询条件
     * @param type $array
     * @return \mysql
     */
    public function where($array) {
        $where = '';
        foreach ($array as $key => $value) {
            if ($where == '') {
                if (is_array($value)) {
                    $where .='';
                } else {
                    $where .= 'where ' . $key . '=' . $value;
                }
            } else {
                if (is_array($value)) {
                    $where .='';
                } else {
                    $where .= 'and ' . $key . '=' . $value;
                }
            }
        }
        $this->_instance['where'] = $where;
        return $this;
    }

    /**
     * 限制
     * @param type $limit
     * @return \mysql
     */
    public function limit($limit) {
        $this->_instance['limit'] = ' limit ' . $limit;
        return $this;
    }

    /**
     * 查询表
     * @param type $table
     * @return \mysql
     */
    public function table($table) {
        $this->_instance['table'] = $table . ' ';
        return $this;
    }

    /**
     * 查询字段
     * @param type $fileds
     * @return \mysql
     */
    public function fields($fileds) {
        $this->_instance['fields'] = 'select ' . $fileds . ' ';
        return $this;
    }

    /**
     * 排序方式
     * @param type $order
     * @return \mysql
     */
    public function orderBy($order) {
        $this->_instance['order'] = 'order by ' . $order . ' ';
        return $this;
    }

    /**
     * 筛选方式
     * @param type $group
     * @return \mysql
     */
    public function groupBy($group) {
        $this->_instance['group'] = 'group by ' . $group . ' ';
        return $this;
    }

    /**
     * 语句组装
     * @return type
     */
    public function select() {
        $sql = '';

        if (isset($this->_instance['fields'])) {
            $sql .= $this->_instance['fields'];
        } else {
            $sql .= 'select * ';
        }
        if (isset($this->_instance['table'])) {
            $sql .= ' from ' . $this->_instance['table'];
        } else {
            die('必须指定查询表');
        }


        if (isset($this->_instance['where'])) {
            $sql .= $this->_instance['where'];
        }
        if (isset($this->_instance['limit'])) {
            $sql .= $this->_instance['limit'];
        }
        if (isset($this->_instance['order'])) {
            $sql .= $this->_instance['order'];
        }
        if (isset($this->_instance['group'])) {
            $sql .= $this->_instance['group'];
        }

        //return $sql;
        //echo $sql;
        $this->query($sql, 'read');
    }

    /**
     * --------------------------------------------------------------
     * 写数据
     * --------------------------------------------------------------
     */
    public function insert($data) {
        $sql = 'insert into ';
        if (isset($this->_instance['table'])) {
            $sql .= $this->_instance['table'];
        } else {
            die('必须指定查询表');
        }

        $data_key = '(';
        $data_value = '';
        foreach ($data as $key => $value) {
            $data_key .= $key . ',';
            $data_value .= $value . ',';
        }

        $data_key = rtrim($data_key, ',') . ')';
        $data_value = rtrim($data_value, ',') . ')';

        $sql .= $data_key . ' values ' . $data_value;

        $this->query($sql, 'write');
    }

}
