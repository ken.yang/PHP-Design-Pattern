<?php

/*
 * 数据库配置文件
 */


/**
 * 普通数据库连接方式
 */
//return array(
//    'default' => array(
//        'servername' => '',
//        'username' => '',
//        'password' => '',
//        'dbname' => ''
//    )
//);


/**
 * 读写分类数据库连接方式
 */
return array(
    'read' => array(
        'servername' => '127.0.0.1',
        'username' => 'root',
        'password' => '123456',
        'dbname' => 'read_db'
    ),
    'write' => array(
        'servername' => '127.0.0.1',
        'username' => 'root',
        'password' => '123456',
        'dbname' => 'write_db'
    )
);
