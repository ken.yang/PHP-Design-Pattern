<?php

/*
 * PHP数组中常用函数练习
 */

/**
 * array_column
 * 返回数组中指定的一列
 * */
$records = array(
	array(
		'id' => 301,
		'first_name' => 'ken',
		'last_name' => 'yang'
	),
	array(
		'id' => 302,
		'first_name' => 'job',
		'last_name' => 'dong'
	),
	array(
		'id' => 303,
		'first_name' => 'join',
		'last_name' => 'tom'
	)
);
$result = array_column($records, 'first_name', 'id');
print_r($result);
//Array ( [301] => ken [302] => job [303] => join )


/**
 * array_combine
 * 创建一个数组，数组A的键值作为键值，数组B的键值作为键名
 * */
$arrayA = array('a' => 'username', 'b' => 'age');
$arrayB = array('ken', 32);
$arrayC = array_combine($arrayA, $arrayB);
print_r($arrayC);
// Array ( [username] => ken [age] => 32 )


/**
 * array_count_values
 * 统计数组中所有的值出现的次数
 * */
$array = array(2, 'ken', 'ken', 2, 'age', 'sex', 2);
print_r(array_count_values($array));
//Array ( [2] => 3 [ken] => 2 [age] => 1 [sex] => 1 )


/**
 * array_key_exists
 * 检查给定的键名或索引是否存在于数组中
 * */
$array = array(
	'username' => 'ken',
	'age' => 23
);
if (array_key_exists('age', $array)) {
	echo '存在age这个键名';
} else {
	echo '不存在age这个键名';
}
//存在age这个键名


/**
 * array_rand
 * 从数组中随机取出一个或多个单元
 * */
$array = array('php', 'java', 'c', 'c++', 'python', 'javascript');
$array_rand_key = array_rand($array, 3);
print_r($array[$array_rand_key[0]] . '<br>');
print_r($array[$array_rand_key[1]] . '<br>');
print_r($array[$array_rand_key[2]] . '<br>');
//以下值为随机产出的，且不会重复
//java
//python
//javascript


/**
 * shuffle
 * 将数组打乱
 * */
$array = array('php', 'java', 'c', 'c++', 'python', 'javascript');
shuffle($array);
print_r($array);
//以下结果为随机出现
//Array ( [0] => c++ [1] => php [2] => c [3] => javascript [4] => java [5] => python )


/**
 * array_search
 * 在数组中搜索给定的值，如果成功则返回相应的键名
 * */
$array = array('php', 'java', 'c', 'c++', 'python', 'javascript');
$key = array_search('c', $array);
print_r($key);
//2


/**
 * array_sum
 * 计算数组中所有值得和
 * */
$array = array(2, 3, 4, 5);
print_r(array_sum($array));
//14


/**
 * array_unique
 * 移除数组中重复的值
 * */
$array = array('a' => 'php', 'b' => 'java', 'c' => 'php', 'd' => 'c++');
print_r(array_unique($array));
//Array ( [a] => php [b] => java [d] => c++ )


/**
 * array_values
 * 返回数组中的所有键值,相当于去掉键名，按照自然序列当键名
 * */
$array = array('a' => 'php', 'b' => 'java', 'c' => 'c++');
print_r(array_values($array));
//Array ( [0] => php [1] => java [2] => c++ )


/**
 * list
 * 把数组中的值赋值给一些变量
 * */
$array = array('php', 'java');
list($valueA, $valueB) = $array;
print_r($valueA);
print_r($valueB);
//php    java